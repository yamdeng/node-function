## 사전 install 및 pm2 실행

*   node 설치
*   node 라이브러리 설치
    *   npm install
*   pm2 설치
    *   npm install -g pm2
*   pm2 실행
    *   pm2 start pm2-config.json

## test url

*   websocket test
    *   http://127.0.0.1:8000/index.html
*   polling test
    *   http://127.0.0.1:8000/polling.html

## npm run script

| index | run name (파일명) | 설명 | test url |
|----------|----------|-------------|-------------|
| 1 | test:cluster (__cluster.js__) | node cluster 기능 테스트 | 해당사항없음 |
| 2 | test:socket.io (__socket.io.js__) | socket.io 기능 테스트 | http://127.0.0.1:8001 |
| 3 | test:socket.io-cluster (__socket.io-cluster.js__) | socket.io + cluster 기능 테스트 | http://127.0.0.1:8002 |
| 4 | test:sticky-session1 (__sticky-session.js__) | socket.io + sticky-session(cluster) 기능 테스트 | http://127.0.0.1:8003 |
| 5 | test:sticky-session2 (__sticky-session2.js__) | socket.io + sticky-session(cluster) 기능 테스트 | http://127.0.0.1:8003 |
| 6 | test:redis (__redis.js__) | socket.io + cluster + redis 기능 테스트 | http://127.0.0.1:8004 |
| 7 | start (__index.js__) | socket.io + sticky-session(cluster) + redis 기능 테스트 | http://127.0.0.1:8000 |

## npm run script 상세 설명

*   1.__test:cluster__ : node에서 기본적으로 제공하는 cluster 기능 테스트
    *   cluster 프로세스 스케줄링은 window은 운영체제, 그외의 OS는 RR 방식임

*   2.___test:socket.io__ : 단일 프로세스 socket.io 기능 테스트
    *   단일 프로세스이여서 테스트 특이사항이 없음

*   3.__test:socket.io-cluster__ : 멀티 프로세스(node cluster 사용) socket.io 기능 테스트
    *   테스트 이슈사항
        *   ㄱ.같은 프로세스(같은 클러스터)인 경우에만 socket 메시지를 전달 받을 수 있음
        *   ㄴ.transport 방식을 명시하지 않을 경우 polling으로 한번 체크해서 ws로 protocol 체인지 하는데 이때 동일한 프로세스(클러스트)에게 요청이 않갈 경우 에러 발생
        *   ㄷ.sticky session을 적용하기 전이라 long polling시 기존에 맺었던 프로세스에게 요청이 가지 않을 경우에 "Session ID unknown" 에러가 발생됨

*   4.__test:sticky-session1__ : 멀티 프로세스(node cluster 사용) socket.io + sticky session 기능 테스트(__sticky-session 라이브러리 사용__)
    *   테스트 이슈사항
        *   ㄱ.같은 프로세스(같은 클러스터)인 경우에만 socket 메시지를 전달 받을 수 있음

*   5.__test:sticky-session2__ : 라이브러리 사용 부분만 틀리고 4와 동일(__socketio-sticky-session 라이브러리 사용__)

*   6.__test:redis__ : 멀티 프로세스(node cluster 사용) socket.io + redis 기능 테스트
    *   테스트 이슈사항
        *   ㄱ.transport 방식을 명시하지 않을 경우 polling으로 한번 체크해서 ws로 protocol 체인지 하는데 이때 동일한 프로세스(클러스트)에게 요청이 않갈 경우 에러 발생
        *   ㄴ.sticky session을 적용하기 전이라 long polling시 기존에 맺었던 프로세스에게 요청이 가지 않을 경우에 "Session ID unknown" 에러가 발생됨

*   7.__start__ : sticky session + redis 기능 테스트
    *   long polling 이슈 success + 다른 클러스터 간의 메시지 전송도 succes

## 결론

*   long polling을 고려하여 sticky-session + redis adapter 셋팅하면 될득함(src/index.js 참고)
*   웹소켓을 지원하지 않는 브라우저를 고려할 필요 없으면 sockec.io client 사용시 transport 옵션을 websocket으로 fix 시키는 것이 좋아보임
*   sticky-session 라이브러리 소스가 적은 편이여서 이슈사항이 발생해도 쉽게 소스의 어떤 부분때문에 동작이 않되는지 체크할 수 있음

## 그외 체크해야 할 사항들

*   proxy server + asw의 alb 연동시 정확하게 동작하는지 확인 필요
*   express session과 같은 부가 기능 사용시 aws와 연계한 socket.io/express cluster 로드밸런싱, socket.io/express proxy server 로드밸런싱 이슈 존재 여부 테스트 필요

## 기타 : redis 단점
 
*   운영 노하우가 없으면 커맨드 같은 부분을 잘 사용해야 한다 : single thread 기반이라 가능하면 운영환경에서는 enduser 사용부분 빼고는 기타 커맨드 사용 X
*   메모리 DB라 용량이 제한이 있어서 고려해서 사용해야 함
*   redis가 모든 소켓정보를 가지고 있게되므로 설치 장치 fail시 모든 통신이 않된다. 장비 fail을 고려한 active/active, active/standby + 데이터 동기화 같은 어려운 이슈 및 비용이 발생할 것으로 보임(확장 및 장애 대응에 비용 발생)
