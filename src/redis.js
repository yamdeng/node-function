'use strict';

const logger = require('./utils/logger');
const cluster = require('cluster');
let clusterForkSize = require('os').cpus().length;
const process = require('process');
const redis = require('socket.io-redis');
const CONFIG = require('./config');

// PORT 아규먼트가 전달이 않되어있을때는 8000 PORT를 default로 server run
let SERVER_PORT = process.env.PORT || 8000;

logger.info('======= cluster.js start : ' + (cluster.isMaster ? 'MASTER' : 'WORKER') + ' =======');
logger.info('cup clusterForkSize : ' + clusterForkSize);
logger.info('process info : [' + process.pid + '], cluster id : [' + (cluster.worker ? cluster.worker.id : 'master') + ']');

let clusterIdArray = [];

if (cluster.isMaster) {
    for (let i = 0; i < clusterForkSize; i++) {
        cluster.fork();
    }
    cluster.on('exit', function (worker) {
        logger.info('worker ' + worker.process.pid + ' died');
    });
    cluster.on('fork', (worker) => {
        clusterIdArray.push(worker.id);
        logger.info('cluster fork id : ' + worker.id, 'red');
    });
} else {

    let app = null;
    let server = null;
    try {
        app = require('./app');
        server = require('http').Server(app);
    } catch (error) {
        logger.error('app init error : ' + error + error.stack ? ' stack : ' + error.stack : '');
        process.exit(-1);
    }

    const io = require('socket.io')(server);
    io.adapter(redis({
        host: CONFIG.REDIS_HOST,
        port: CONFIG.REDIS_PORT
    }));
    io.on('connection', function (socket) {
        logger.info('process id : [' + process.pid + '], cluster id : [' + cluster.worker.id + ']' + 'socket connected : ' + socket.id);
        socket.on('chat message', function (msg) {
            logger.info('[' + process.pid + '] [' + cluster.worker.id + '] [' + socket.id + ']: ' + msg, 'blue');
            io.emit('chat message', '[' + process.pid + '] [' + cluster.worker.id + '] [' + socket.id + ']: ' + msg);
        });
        socket.on('disconnect', function () {
            logger.info('socket disconnect : ' + socket.id);
        });
    });
    server.listen(SERVER_PORT, () => {
        logger.info('socket.io.server start : ' + SERVER_PORT);
    });

}
