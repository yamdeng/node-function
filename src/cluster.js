const logger = require('./utils/logger');
const cluster = require('cluster');
const clusterForkSize = require('os').cpus().length;
const process = require('process');
const express = require('express');
const SERVER_PORT = process.env.PORT || 8091;

logger.info('======= cluster.js start : ' + (cluster.isMaster ? 'MASTER' : 'WORKER') + ' =======');
logger.info('cup clusterForkSize : ' + clusterForkSize);
logger.info('process info : [' + process.pid + '], cluster id : [' + (cluster.worker ? cluster.worker.id : 'master') + ']');

let clusterIdArray = [];

if (cluster.isMaster) {
    for (let i = 0; i < clusterForkSize; i++) {
        cluster.fork();
    }
    cluster.on('exit', function (worker) {
        logger.info('worker ' + worker.process.pid + ' died');
    });
    cluster.on('fork', (worker) => {
        clusterIdArray.push(worker.id);
        logger.info('cluster fork id : ' + worker.id, 'red');
        worker.send('good fork : ' + worker.id);
        if (worker.id === 1 || worker.id === 2) {
            worker.destroy();
        }
    });
    for (const id in cluster.workers) {
        cluster.workers[id].on('message', (message) => {
            logger.info(message);
        });
    }
} else {
    logger.info('run process info : ' + process.pid, 'red');
    logger.info('cluster fork() call : ' + cluster.worker.id);

    let app = express();
    app.get('/', function (req, res) {
        logger.info('execute worker pid is ' + process.pid, 'red');
        res.send('process id : [' + process.pid + '], cluster id : [' + cluster.worker.id + ']');
    });

    app.listen(SERVER_PORT, () => {
        logger.info('start server ' + SERVER_PORT);
    });

    process.on('message', function (masterSendMessage) {
        logger.info('cluster id is ' + cluster.worker.id + ' receive message : ' + masterSendMessage);
    });

    process.send(cluster.worker.id + ' server start complete');

}
